import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.junit.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by mbuettner on 14.11.2016.
 * designed for Testing via Appium Server
 */
public class Basistest {
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() throws MalformedURLException {

        //Set GeckoDriver (Marionette Driver) path as system property
        System.setProperty("webdriver.gecko.driver","C:\\tools\\selenium\\geckodriver\\geckodriver-v0.10.0-win64\\geckodriver.exe");

        File appDir = new File("src");

        File app = new File(appDir, "app-debug.apk");

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
        //hier bei der Angabe "Android device" oder "Android device" kann man die Testfälle entweder über Emilator starten
        //oder über Real-Android System (Auf Test-Gerät) ausführen.
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");

        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100");
        cap.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());

        AndroidDriver driver= new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);


    }
