import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by mbuettner on 09.11.2016.
 */
public class SF_HomepageTest {
    // a static driver variable
    static WebDriver driver;
    String homepage = "https://sfsitecoredev.westeurope.cloudapp.azure.com";
    WebDriverWait wait = new WebDriverWait(driver,10);

        @BeforeClass
        public static void setupTest() {
        //Set GeckoDriver (Marionette Driver) path as system property
        System.setProperty("webdriver.gecko.driver", "C:\\tools\\selenium\\geckodriver\\geckodriver-v0.10.0-win64\\geckodriver.exe");
        driver = new FirefoxDriver();
        //driver.manage().getCookies();
    }
        @Test
        //Execute testcase "TF_01_CMS" (Rubrik "Service&Rathaus" aufrufen)
        public void TF_01_CMS () {
            // Open URL at first time
            driver.get(homepage);
            //Check Website Title is korrect
            Assert.assertTrue(driver.getTitle().contentEquals("FRANKFURT.DE - DAS OFFIZIELLE STADTPORTAL"));

            //**OPTIONAL:** Check some Items are visible
            driver.findElement(By.xpath(".//*[@id='mainnavigationBox']/div/div[2]")).getAttribute("row headerBox");
            driver.findElement(By.xpath(".//*[@id='headerBox']/img")).getAttribute("headerBoxImage");

            String xpathToolbarMenu = "//*[@id='mainnavigationBox']/div/div[2]/div/div[1]/a[2]/div[2]";
            //Find Toolbar "Menu" and click
            WebElement toolbar_Menu = driver.findElement((By.xpath(xpathToolbarMenu)));
            toolbar_Menu.click();
            
            //WebbrowerWait 2Seconds
            driver.manage().timeouts().implicitlyWait(2,SECONDS);

            String xpathServRath = "/html/body/div[3]/div/div/div/div/div[3]/div[1]/a";
            //Click on Element "Service & Rathaus"
            WebElement ServRath = driver.findElement(By.xpath(xpathServRath));
            ServRath.click();

            String xpathRathaus = ".//*[@id='mainContent']/div[2]/div[2]/div[1]";
            //Find WebElement Uebersicht SERVICE & RATHAUS
            WebElement pageRathaus = driver.findElement(By.xpath(xpathRathaus));
            System.out.println(pageRathaus);
        }

        @Test
        //Execute testcase "TF_02_CMS" ("MenuBox schliessen")
        public void TF_02_CMS() {
            //Back to Starting URL
            driver.navigate().to(homepage);

            String xpathToolbarMenu = ".//*[@id='mainnavigationBox']/div/div[2]/div/div[1]/a[2]/div[2]";
            //Find Toolbar "MENU"
            WebElement toolbar_menu = driver.findElement((By.xpath(xpathToolbarMenu)));
            toolbar_menu.click();
            //WebbrowerWait 2 Seconds
            driver.manage().timeouts().implicitlyWait(2,SECONDS);

            String xpathMenuCancel = "/html/body/div[3]/div/div/div/div/div[1]/div[2]/a";
            //Find WebElement Button "MENU Cancel"
            WebElement menu_cancel;
            menu_cancel = driver.findElement(By.xpath(xpathMenuCancel));
            menu_cancel.click();
        }

        @Test
        //Execute testcase "TF_03_CMS" (Menubox mir ESC-Taste schliessen)
        public void TF_03_CMS () {
            //Back to Starting URL
            driver.navigate().to(homepage);

            String xpathToolbarMenu = ".//*[@id='mainnavigationBox']/div/div[2]/div/div[1]/a[2]/div[2]";
            //Find WebElement Toolbar "MENU"
            WebElement toolbar_menu ;
            toolbar_menu = driver.findElement((By.xpath(xpathToolbarMenu)));
            toolbar_menu.click();
            //WebbrowerWait 2Seconds
            driver.manage().timeouts().implicitlyWait(2,SECONDS);
            //Aborting with Key "ESCAPE" while processing Toolbox "MENU"
            toolbar_menu.sendKeys(Keys.ESCAPE);
        }

        @Test
        //Execute testcase "TF_04_CMS" (nach "Ordungsamt" suchen)
        public void TF_04_CMS () {
            //Back to Starting URL
            driver.navigate().to(homepage);

            String xpathSuche = ".//*[@id='searchIcon']/a[2]";
            //FindElement "suche" in Toolbar
            WebElement suche;
            suche = driver.findElement((By.xpath(xpathSuche)));
            suche.click();
            //Wait 2 seconds
            driver.manage().timeouts().implicitlyWait(2,SECONDS);;

            String xpathSuchfeld = ".//*[@id='searchBox']/form/div/div[1]/input";
            //FindElement "suchfeld"
            WebElement suchfeld;
            suchfeld = driver.findElement(By.xpath(xpathSuchfeld));
            //Type into Field value "ordnungsamt"
            suchfeld.sendKeys("ordnungsamt");

            //** NOCH ZU REFACTORING: warte 10sekunden, bis im WebElement suchfeld der Value "ordnungsamt" eingetragen wurde**
            //wait.until(ExpectedConditions.textToBePresentInElement(suchfeld,"ordnungamt"));

            String xpathSubmit = "/html/body/div[2]/div[1]/div[4]/div/div/div[2]/div/div[3]/form/div/div[3]/button";
            // Find Webelement "submit"
            WebElement submit = driver.findElement(By.xpath(xpathSubmit));
            //Confirm and send this request
            submit.click();
            //Wait 2 seconds
            driver.manage().timeouts().implicitlyWait(2,SECONDS);

            String xpathSearchResult = "/html/body/div[2]/div[2]/div/div/div[2]/div/div[2]/div";
            //Find Webelement searchResult
            WebElement searchResult = driver.findElement(By.xpath(xpathSearchResult));
            searchResult.getText();
            // WebbrowerWait implicitly Seconds
            wait.withTimeout(10,SECONDS);
            System.out.println(searchResult);
        }

        @Test
        //Execute TF_05_CMS (Submenu "Kontakt" aufrufen)
        public void TF_05_CMS () {
            wait.withTimeout(10,SECONDS);
            //Back to Starting URL
            driver.navigate().to(homepage);

            String xpathKontakt = "html/body/div[2]/div[1]/div[2]/div/div[2]/ul/li[4]/a";
            //Find WebElement "kontakt" in header
           WebElement button_kontakt = driver.findElement(By.xpath(xpathKontakt));
           //Click on button_kontakt
            button_kontakt.click();
           // Check Website page_kontakt is visible
           WebElement page_kontakt = driver.findElement(By.cssSelector(".contentBox.shadowBox"));
           page_kontakt.getText();
        }

        @AfterClass
        public static void FinishedTest(){
        driver.manage().deleteAllCookies();
        //driver.quit();
        driver.close();

    }


    }